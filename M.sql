--
-- File generated with SQLiteStudio v3.4.4 on ศ. ต.ค. 6 19:01:46 2023
--
-- Text encoding used: UTF-8
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: MATERIAL
DROP TABLE IF EXISTS MATERIAL;

CREATE TABLE IF NOT EXISTS MATERIAL (
    M_CODE           INTEGER        PRIMARY KEY AUTOINCREMENT
                                    UNIQUE
                                    NOT NULL,
    M_NAME           VARCHAR (20)   NOT NULL,
    M_QOH            NUMERIC (6, 2) CHECK (M_QOH >= 0.0 AND 
                                           M_QOH <= 9999.0) 
                                    NOT NULL,
    M_PRICE_PER_UNIT NUMERIC (6, 2) CHECK (M_PRICE_PER_UNIT >= 0.0 AND 
                                           M_PRICE_PER_UNIT <= 9999.0) 
                                    NOT NULL,
    M_UNIT           VARCHAR (20)   NOT NULL,
    M_MIN            NUMERIC (6, 2) CHECK (M_MIN >= 0.0 AND 
                                           M_MIN <= 9999.0) 
                                    NOT NULL
);


-- Index: sqlite_autoindex_MATERIAL_1
DROP INDEX IF EXISTS sqlite_autoindex_MATERIAL_1;

CREATE UNIQUE INDEX IF NOT EXISTS sqlite_autoindex_MATERIAL_1 ON MATERIAL (
    M_CODE COLLATE BINARY
);


COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
