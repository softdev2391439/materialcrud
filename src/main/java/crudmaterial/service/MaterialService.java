/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package crudmaterial.service;

import crudmaterial.dao.MaterialDao;
import crudmaterial.model.Material;
import java.util.List;

/**
 *
 * @author Nobpharat
 */
public class MaterialService {   
    public Material getById(int id){
        MaterialDao materialDao = new MaterialDao();
        return materialDao.get(id);
    }
    public List<Material> getMaterials(){
        MaterialDao materialDao = new MaterialDao();
        
        return materialDao.getAll(" M_CODE asc");
    }

    public Material addNew(Material editedMaterial) {
       MaterialDao materialDao = new MaterialDao();
        return materialDao.save(editedMaterial);
    }

    public Material update(Material editedMaterial) {
        MaterialDao materialDao = new MaterialDao();
        return materialDao.update(editedMaterial);
    }

    public int delete(Material editedMaterial) {
        MaterialDao materialDao = new MaterialDao();
        return materialDao.delete(editedMaterial);
    }
}

