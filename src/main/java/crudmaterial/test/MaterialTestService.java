/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package crudmaterial.test;

import crudmaterial.model.Material;
import crudmaterial.service.MaterialService;

/**
 *
 * @author Nobpharat
 */
public class MaterialTestService {
    public static void main(String[] args) {
        MaterialService mat = new MaterialService();
        for(Material material : mat.getMaterials()){
            System.out.println(material );
        }
        System.out.println(mat.getById(1));
        Material newmaMaterial = new Material("Coco", "Kilogram", 10, 20, 20);
        mat.addNew(newmaMaterial);
        for(Material material : mat.getMaterials()){
            System.out.println(material );
        }
        
        Material upMaterial = mat.getById(2);
        upMaterial.setMinimum(100.50);
        mat.update(upMaterial);
        System.out.println(mat.getById(2));
        mat.delete(mat.getById(4));
        for(Material material : mat.getMaterials()){
            System.out.println(material );
        }
        
    }
}
